# XML XSLT #

Projekt jest dostępny [tutaj](http://ux.up.krakow.pl/~kwilas/PKL/pkl.xml).

Projekt prezentuje użycie zdefiniowanych przez użytkownika znaczników w standardzie XML. Przedstawia opis ośrodków PKL z roku 2016.

XSL opisuje jak będą przedstawione dane z pliku XML. Wszystkie pliki przeszły walidację i są zgodne ze standardami W3C.

Zamieszczony został również przykład pliku schematu - xsd oraz dtd.