<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="5.0" encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="content-type" content="text/html; charset=UTF-8">
                    <title>PKL</title>
                </meta>
                <link rel="stylesheet" type="text/css" href="styl.css"/>
                <script type="text/javascript"
                        src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                <script charset="utf-8">
                    function slide(x) {
                    $("#"+x).slideToggle();
                    }
                </script>
            </head>
            <body>

                <div id="glowne">
                    <div id="glowa">
                        <div id="logo">

                        </div>
                        <div id="menu">
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol1</xsl:text>
                                        </xsl:attribute>
                                        Kasprowy Wierch
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol2</xsl:text>
                                        </xsl:attribute>
                                        Mosorny Groń
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol3</xsl:text>
                                        </xsl:attribute>
                                        Gubałówka
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol4</xsl:text>
                                        </xsl:attribute>
                                        Góra Parkowa
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol5</xsl:text>
                                        </xsl:attribute>
                                        Palenica
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol6</xsl:text>
                                        </xsl:attribute>
                                        Góra Żar
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>#lol7</xsl:text>
                                        </xsl:attribute>
                                        Jaworzyna
                                    </a>
                                </li>


                            </ul>
                        </div>
                    </div>


                    <div id="srodek2">

                        <div id="srodek_zawartosc">
                            <div id="tresc">

                                <div id="bzdety">

                                    <b>Polskie Koleje Linowe SA</b>
                                    – nieistniejący operator przewozów osobowych w regionie górskim, powstały wskutek
                                    restrukturyzacji i komercjalizacji Polskich Kolei Państwowych. Z dniem 30 czerwca
                                    2015 roku na podstawie art. 492 par. 1 pkt. 1 Kodeksu Spółek Handlowych nastąpiło
                                    połączenie spółki Polskie Koleje Linowe SA z siedzibą w Zakopanem (spółka
                                    przejmowana), ze spółką Polskie Koleje Górskie SA z siedzibą w Zakopanem (spółka
                                    przejmująca) i w konsekwencji wykreślenie PKL SA z KRS.

                                    Przed restrukturyzacją firma zatrudniała 210 pracowników, posiadała 9 kolei, 7
                                    wyciągów, 5 zjeżdżalni, 100 ha tras narciarskich i przewozi rocznie 6 mln pasażerów
                                    (w ciągu 75 lat istnienia – 225 milionów pasażerów).

                                    <br></br>
                                    Chronologia wydarzeń:

                                    <li>1936 – uruchomienie kolejki linowej na Kasprowy Wierch w Zakopanem</li>
                                    <li>1937 – uruchomienie kolejki linowo-terenowej na Górę Parkową w Krynicy</li>
                                    <li>1938 – uruchomienie kolejki linowo-terenowej na Gubałówkę w Zakopanem</li>
                                    <li>1938 – uruchomienie wyciągu saniowego z Kotła Gąsienicowego na Kasprowy Wierch,
                                        który w 1962 został zastąpiony kolejką krzesełkową
                                    </li>
                                    <li>1939–1945 – zarządzanie kolejką na Kasprowy przez niemiecką Kolej Wschodnią
                                        (Ostbahn)
                                    </li>
                                    <li>1945–1947 – funkcjonowanie w strukturze Państwowego Zarządu Przymusowego nad
                                        Urządzeniami Turystycznymi Ministerstwa Komunikacji
                                    </li>
                                    <li>1947 – utworzenie przedsiębiorstwa pomocniczego pn. Państwowe Koleje Linowe, ich
                                        upaństwowienie i włączenie w strukturę PKP
                                    </li>
                                    <li>1953 – uruchomienie kolejki linowej gondolowej na Szyndzielnię, koło
                                        Bielska-Białej
                                    </li>
                                    <li>1962 – uruchomienie kolejki z krzesełkami jednoosobowymi w Kotle Gąsienicowym
                                    </li>
                                    <li>1969 – uruchomienie kolejki z krzesełkami dwuosobowymi w Kotle Goryczkowym</li>
                                    <li>1977 – uruchomienie kolejki krzesełkowej na Butorowy Wierch w Kościelisku koło
                                        Zakopanego
                                    </li>
                                    <li>1991 – uruchomienie kolejki z krzesełkami dwuosobowymi na Palenicę w
                                        Szczawnicy
                                    </li>
                                    <li>2000 – restrukturyzacja podmiotu PKP Państwowe Koleje Linowe i przekształcenie w
                                        Polskie Koleje Linowe Spółka z o.o.
                                    </li>
                                    <li>2000 – uruchomienie kolejki krzesełkowej na Hali Gąsienicowej</li>
                                    <li>2001 – modernizacja kolejki linowo-terenowej na Gubałówkę w Zakopanem</li>
                                    <li>2002 – wpis do rejestru przedsiębiorców</li>
                                    <li>2003 – uruchomienie kolei terenowej na górę Żar w Międzybrodziu Żywieckim</li>
                                    <li>2005 – uruchomienie nowej czteroosobowej kolei krzesełkowej (wyprzęganej) na
                                        Palenicę w Szczawnicy.
                                    </li>
                                    <li>2007 – uruchomienie zmodernizowanej kolei linowej na Kasprowy Wierch</li>
                                    <li>2008 – przekształcenie PKL Sp. z o.o. w PKL SA</li>
                                    <li>2010 – zakup Ośrodka Turystyczno-Narciarskiego Mosorny Groń w Zawoi w Zawoi z
                                        kolejką krzesełkową
                                    </li>
                                    <li>2013 – sprzedaż PKL SA spółce Polskie Koleje Górskie SA (99,77% udziałów spółka
                                        Altura założona przez fundusz inwestycyjny Mid Europa Partners, 0,23% udziałów
                                        cztery gminy z terenu powiatu tatrzańskiego – Gmina Miasto Zakopane, Gmina
                                        Bukowina Tatrzańska, Gmina Kościelisko i Gmina Poronin)
                                    </li>
                                    <li>2016 – spodziewane rozwiązanie spółki, po połączeniu z PKG SA.</li>


                                </div>


                            </div>


                            <div id="tresc2">
                                <div id="naglowek">
                                    Wydarzenia
                                </div>
                                <div id="boczek">
                                    <div class="zawartosc2">

                                        <xsl:for-each select="document('wydarzenia.xml')/wydarzenia/wydarzenie">
                                            <xsl:sort select="dzien" data-type="number" order="ascending"/>
                                            <xsl:call-template name="bok"></xsl:call-template>
                                        </xsl:for-each>

                                    </div>
                                </div>

                                <div id="naglowek">Pogoda
                                    <br></br>
                                    <br></br>
                                </div>
                                <div id="boczek">
                                    <div class="zawartosc2">
                                        <br></br>
                                        <br></br>

                                        <a href="http://www.twojapogoda.pl/">
                                            <img src="chmura.jpg" height="200" width="200"/>
                                        </a>

                                        <ul id="menu1"></ul>


                                    </div>
                                </div>

                                <div id="naglowek">
                                    FB
                                </div>
                                <div id="boczek">
                                    <div class="zawartosc2">


                                        <ul id="menu1">
                                            <br></br>
                                            <br></br>

                                            <li>
                                                <a href="#">
                                                    <img src="fb.png" height="200" width="200"/>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="https://www.facebook.com/PolskieKolejeLinowe/?fref=ts">
                                                            Kasprowy Wierch
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/PKLMosornyGron/?fref=ts">
                                                            Mosorny Groń
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/PKLGubalowka/?fref=ts">
                                                            Gubałówka
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/PKLGoraParkowa/?fref=ts">Góra
                                                            Parkowa
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/PKLPalenica/?fref=ts">
                                                            Palenica
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/PKLGoraZar/?fref=ts">Góra
                                                            Żar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/stacjanarciarska/">Jaworzyna
                                                            Krynicka
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>


                                    </div>


                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                </div>

                            </div>
                        </div>

                    </div>


                    <div id="srodek">

                        <div id="srodek_zawartosc">
                            <div id="tresc">

                                <div id="naglowek">
                                    Obiekty Polskich Kolei Linowych
                                </div>

                                <div class="zawartosc">

                                    <xsl:for-each select="/pkl/kolejka">

                                        <div>
                                            <xsl:attribute name="id">
                                                <xsl:value-of select="ino"/>
                                            </xsl:attribute>
                                        </div>

                                        <xsl:call-template name="kolejka"></xsl:call-template>
                                    </xsl:for-each>

                                </div>


                            </div>


                        </div>

                    </div>

                    <div id="stopka">
                        <div class="zawartosc" style="padding-top:20px;">
                            <font style="font-size:14pt;">2016 <b>PKL</b> Projekt zaliczeniowy z przedmiotu: XML i
                                technologie pokrewne <br></br>Kamil Wilas i Janusz Wiejowski
                            </font>
                        </div>
                    </div>
                </div>

            </body>
        </html>

    </xsl:template>


    <xsl:template name="bok">
        <xsl:if test="position() &lt;= 5">
            <div id="lista">
                <font style="font-size:10pt;">
                    <b>
                        <xsl:value-of select="dzien"/><xsl:value-of select="data"/>
                        <br/>
                        <xsl:value-of select="godzina"/>:00
                    </b>
                    -
                    <xsl:value-of select="pkl/nazwa"/>
                </font>
                <br/>"
                <b>
                    <xsl:value-of select="tytul"/>
                </b>
                "
                <br/>
                <xsl:value-of select="opis"/>
            </div>
        </xsl:if>
    </xsl:template>


    <xsl:template name="kolejka">


        <div id="kolejka">
            <xsl:attribute name="onclick">
                <xsl:text>slide('</xsl:text><xsl:value-of select="id"/><xsl:text>')</xsl:text>
            </xsl:attribute>
            <b>
                <xsl:value-of select="nazwa"/>
            </b>
        </div>
        <div>
            <xsl:attribute name="id">
                <xsl:value-of select="id"/>
            </xsl:attribute>
            <div class="zawartosc">
                <br></br>

                <!-- pierwszy-->

                <div id="kolejka">
                    <xsl:attribute name="onclick">
                        <xsl:text>slide('</xsl:text><xsl:value-of select="raz/id"/><xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <b>
                        <xsl:value-of select="raz/nazwa"/>
                    </b>
                </div>
                <div>
                    <xsl:attribute name="id">
                        <xsl:value-of select="raz/id"/>
                    </xsl:attribute>
                    <div class="zawartoscx">
                        <xsl:value-of select="raz/opis"/>

                    </div>
                </div>


                <!--drugi-->
                <div id="kolejka">
                    <xsl:attribute name="onclick">
                        <xsl:text>slide('</xsl:text><xsl:value-of select="dwa/id"/><xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <b>
                        <xsl:value-of select="dwa/nazwa"/>
                    </b>
                </div>
                <div>
                    <xsl:attribute name="id">
                        <xsl:value-of select="dwa/id"/>
                    </xsl:attribute>
                    <div class="zawartoscx">

                        <TABLE border="2">
                            <TR color="red">
                                <TD>RODZAJ BILETU</TD>
                                <TD>NORMALNY</TD>
                                <TD>ULGOWY</TD>
                            </TR>
                            <TR>
                                <TD>BILET W OBIE STRONY</TD>
                                <xsl:choose>
                                    <xsl:when test="dwa/cena/obie/normalny &gt;= 50">
                                        <td style="color:red">
                                            <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ
                                        </td>
                                    </xsl:when>
                                    <xsl:when test="dwa/cena/obie/normalny &gt; 20">
                                        <td style="color:orange">
                                            <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ
                                        </td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <td style="color:green">
                                            <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ
                                        </td>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <TD><xsl:value-of select="dwa/cena/obie/ulgowy"/>ZŁ
                                </TD>
                            </TR>
                            <TR>
                                <TD>BILET TYLKO W GÓRĘ</TD>
                                <TD>
                                    <xsl:if test="dwa/cena/gora/normalny &gt;= 10">
                                        <xsl:value-of select="dwa/cena/gora/normalny"/>
                                    </xsl:if>
                                    ZŁ
                                </TD>
                                <TD><xsl:value-of select="dwa/cena/gora/ulgowy"/>ZŁ
                                </TD>
                            </TR>
                            <TR>
                                <TD>BILET TYLKO W DÓŁ</TD>
                                <TD><xsl:value-of select="dwa/cena/dol/normalny"/>ZŁ
                                </TD>
                                <TD><xsl:value-of select="dwa/cena/dol/ulgowy"/>ZŁ
                                </TD>
                            </TR>


                            <!--
                          <xsl:choose>
                                  <xsl:when test="normalny &gt; 50">
                                    <td bgcolor="red">
                                    <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ</td>
                                  </xsl:when>
                                  <xsl:when test="price &gt; 20">
                                    <td bgcolor="orange">
                                    <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ></td>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <td bgcolor="green">
                                    <xsl:value-of select="dwa/cena/obie/normalny"/>ZŁ></td>
                                  </xsl:otherwise>
                          </xsl:choose>

                          -->

                        </TABLE>
                    </div>


                </div>
                <!-- trzeci-->


                <div id="kolejka">
                    <xsl:attribute name="onclick">
                        <xsl:text>slide('</xsl:text><xsl:value-of select="trzy/id"/><xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <b>
                        <a>
                            <xsl:attribute name="href">
                                <xsl:value-of select="trzy/link"/>
                            </xsl:attribute>
                            Galeria
                        </a>
                    </b>
                </div>

                <br></br>
            </div>
        </div>

    </xsl:template>
</xsl:stylesheet>